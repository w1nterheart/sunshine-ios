//
//  HomeViewController.h
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/15/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface HomeViewController : UIViewController <CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) CLLocationManager *locationManager;
@end
