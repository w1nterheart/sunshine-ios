
//
//  DetailViewController.m
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/15/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import "DetailViewController.h"
#import "Utility.h"

@interface DetailViewController () {
    WeatherEntry *_data;
    
    UIImageView *_forecastIcon;
    UILabel *_maxTempTextField;
    UILabel *_minTempTextField;
    UILabel *_dateTextField;
    UILabel *_forecastTextField;
    UILabel *_humidityLabel;
    UILabel *_windLabel;
    UILabel *_pressureLabel;
}
@end

@implementation DetailViewController

//@synthesize dateTextField = _dateTextField;
//@synthesize forecastTextField = _forecastTextField;
//@synthesize maxTempTextField = _maxTempTextField;
//@synthesize minTempTextField = _minTempTextField;
//@synthesize forecastIcon = _forecastIcon;
//@synthesize pressureLabel = _pressureLabel;
//@synthesize windLabel = _windLabel;
//@synthesize humidityLabel = _humidityLabel;

- (id) initWithWeatherEntry: (WeatherEntry *) weatherEntry {
    self = [super init];
    _data = weatherEntry;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.clipsToBounds = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    
    CGSize size = self.view.bounds.size;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    //Navbar color
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        // iOS 7.+
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:40./255. green:185./255. blue:244./255. alpha:1.];
        self.navigationController.navigationBar.translucent = NO;
    } else {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:40./255. green:185./255. blue:244./255. alpha:1.];
    }
    
    _dateTextField = [[UILabel alloc] initWithFrame: CGRectMake(0., 0., size.width, 32.)];
    [_dateTextField setFont: [UIFont boldSystemFontOfSize: 14.f]];
    [_dateTextField setTextAlignment:NSTextAlignmentLeft];
    [_dateTextField setTextColor:[UIColor blackColor]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat: @"EEEE, MMMM dd"];
    _dateTextField.text = [formatter stringFromDate: _data.date];
    [self.view addSubview: _dateTextField];
    
    _forecastIcon = [[UIImageView alloc] initWithFrame: CGRectMake(size.width/2., 32., size.width/2., size.width/2.)];
    [_forecastIcon setImage: [UIImage imageNamed:
                                  [NSString stringWithFormat: @"%@%@",
                                   [[Utility sharedInstance] getIconResourceForWeatherCondition: [_data.status intValue]],
                                   @"_today"]]];
    [self.view addSubview: _forecastIcon];
    
    _forecastTextField = [[UILabel alloc] initWithFrame: CGRectMake(size.width/2., size.width/2. + 32., size.width/2, 24.)];
    [_forecastTextField setFont: [UIFont fontWithName:@"HelveticaNeue" size: 16.f]];
    [_forecastTextField setTextAlignment:NSTextAlignmentCenter];
    [_forecastTextField setTextColor:[UIColor blackColor]];
    _forecastTextField.text = _data.descriptionText;
    [self.view addSubview: _forecastTextField];
    
    _maxTempTextField = [[UILabel alloc] initWithFrame: CGRectMake(0., 32., size.width/2., size.width/4.)];
    [_maxTempTextField setFont:[UIFont boldSystemFontOfSize: 56.f]];
    [_maxTempTextField setTextAlignment:NSTextAlignmentRight];
    [_maxTempTextField setTextColor:[UIColor blackColor]];
    _maxTempTextField.text = [NSString stringWithFormat: @"%@°", _data.max];
    [self.view addSubview: _maxTempTextField];
    
    _minTempTextField = [[UILabel alloc] initWithFrame: CGRectMake(0., size.width/4., size.width/2., size.width/4.)];
    [_minTempTextField setFont:[UIFont fontWithName:@"HelveticaNeue" size: 40.f]];
    [_minTempTextField setTextAlignment:NSTextAlignmentRight];
    [_minTempTextField setTextColor:[UIColor grayColor]];
    _minTempTextField.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    _minTempTextField.text = [NSString stringWithFormat: @"%@°", _data.min];
    [self.view addSubview: _minTempTextField];
    
    _humidityLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., size.height/2., size.width, 32.)];
    [_humidityLabel setFont: [UIFont fontWithName: @"HelveticaNeue" size: 18.f]];
    [_humidityLabel setTextAlignment: NSTextAlignmentLeft];
    [_humidityLabel setTextColor: [UIColor blackColor]];
    _humidityLabel.text = [NSString stringWithFormat: @"Humidity: %@%@", _data.humidity, @"%"];
    [self.view addSubview: _humidityLabel];
    
    _pressureLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., size.height/2.+32., size.width, 32.)];
    [_pressureLabel setFont: [UIFont fontWithName: @"HelveticaNeue" size: 18.f]];
    [_pressureLabel setTextAlignment: NSTextAlignmentLeft];
    [_pressureLabel setTextColor: [UIColor blackColor]];
    _pressureLabel.text = [NSString stringWithFormat: @"Pressure: %@ hPa", _data.pressure];
    [self.view addSubview: _pressureLabel];
    
    _windLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., size.height/2.+64., size.width, 32.)];
    [_windLabel setFont: [UIFont fontWithName: @"HelveticaNeue" size: 18.f]];
    [_windLabel setTextAlignment: NSTextAlignmentLeft];
    [_windLabel setTextColor: [UIColor blackColor]];
    _windLabel.text = [NSString stringWithFormat: @"Wind: %@ km/h %@", _data.windSpeed, _data.windDirection];
    [self.view addSubview: _windLabel];
}

- (void) setWeatherData:(WeatherEntry *) weatherEntry {
    _forecastTextField.text = [NSString stringWithFormat:@"%@", weatherEntry.descriptionText];
    _maxTempTextField.text = [NSString stringWithFormat:@"%@%@", weatherEntry.max, @"°"];
    _minTempTextField.text = [NSString stringWithFormat:@"%@%@", weatherEntry.min, @"°"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end