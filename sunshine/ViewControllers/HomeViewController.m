//
//  HomeViewController.m
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/15/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import "HomeViewController.h"
#import "MFSideMenu.h"
#import "DetailViewController.h"

#import "WeatherEntryTableViewCell.h"
#import "WeatherEntryTodayTableViewCell.h"

#import "WeatherEntry.h"
#import "ConnectionManager.h"
#import "WeatherEntryManager.h"
#import "Utility.h"

#define NAVIGATION_BUTTON_MARGIN ( 5.f )
#define NAVIGATION_ICON_BUTTON_SIDE ( 44.f - NAVIGATION_BUTTON_MARGIN * 2)
#define NAVIGATION_CANCEL_BUTTON_WIDTH ( 60.f )

@interface HomeViewController () <CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate> {
    UIView *_searchView;
    UIButton *_leftMenuButton;
    UIButton *_searchButton;
    UITextField *_searchTextField;
    UIButton *_searchCancelButton;
    UIButton *_searchCancelBarButton;
    UIButton *_addButton;
    UIButton *_settingsButton;
    UITableView *_tableView;
    UILabel *_emptyContentLabel;
    
    NSMutableArray *_data;
    NSMutableArray *weatherEntryArray;
    
    UIRefreshControl *_refreshControl;
    NSString *_theLocation;
    NSArray *_ver;
    BOOL _isUpadtingLocation;
}
//@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupMenuBarButtonItems];
    
    self.view.backgroundColor = [UIColor blackColor];
    self.title = @"Sunshine";
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    //Navbar color
    _ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[_ver objectAtIndex:0] intValue] >= 7) {
        // iOS 7.0+
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:40./255. green:185./255. blue:244./255. alpha:1.];
        self.navigationController.navigationBar.translucent = NO;
    } else {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:40./255. green:185./255. blue:244./255. alpha:1.];
    }
    
    _leftMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _leftMenuButton.frame = CGRectMake(NAVIGATION_BUTTON_MARGIN, NAVIGATION_BUTTON_MARGIN, NAVIGATION_ICON_BUTTON_SIDE, NAVIGATION_ICON_BUTTON_SIDE);
    [_leftMenuButton setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
    [_leftMenuButton addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
//    [self.navigationController.navigationBar addSubview:_leftMenuButton];
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;

    
    [self.view addSubview: _tableView];

    _refreshControl = [[UIRefreshControl alloc] init];
    _refreshControl.backgroundColor = [UIColor colorWithRed:40./255. green:185./255. blue:244./255. alpha:1.];
    [_refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview: _refreshControl];
    
    _data =[[WeatherEntryManager sharedInstance] weatherEntries];
    [_tableView reloadData];
    [self refreshTable];
    
    _isUpadtingLocation = NO;
    
#warning custom cell is registered here
//    [_tableView registerClass:[WeatherEntryTableViewCell class] forCellReuseIdentifier:@"Cell"];

//    _addButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    _addButton.frame = CGRectMake(self.navigationController.navigationBar.bounds.size.width - NAVIGATION_ICON_BUTTON_SIDE - NAVIGATION_BUTTON_MARGIN, NAVIGATION_BUTTON_MARGIN, NAVIGATION_ICON_BUTTON_SIDE, NAVIGATION_ICON_BUTTON_SIDE);
//    [_addButton setTitle:@"+" forState:UIControlStateNormal];
//    _addButton.backgroundColor = [UIColor clearColor];
//    [self.navigationController.navigationBar addSubview:_addButton];
    
    
}

- (void)deviceLocation: (CLLocation *) location {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation: location completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placemark = error ? nil : [placemarks objectAtIndex:0];
        _theLocation = error ?
            [[NSUserDefaults standardUserDefaults] stringForKey:@"locality_preference"] :
            [NSString stringWithFormat: @"%@, %@", placemark.locality, placemark.ISOcountryCode];
        [self refreshTableData];
    }];
    
    NSLog(@"Geocode: %@", _theLocation);
}

- (void) refreshTable {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"location_service_preference"]) {
        if (self.locationManager == nil) {
            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.distanceFilter = kCLDistanceFilterNone;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
            self.locationManager.delegate = self;

        }
        
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
            [self.locationManager requestWhenInUseAuthorization];
        }
        [self.locationManager startUpdatingLocation];
        return;
    } else if ([[NSUserDefaults standardUserDefaults] stringForKey:@"locality_preference"] != NULL) {
        _theLocation = [[NSUserDefaults standardUserDefaults] stringForKey:@"locality_preference"];
    } else {
        _theLocation = @"Mountain%20View,United%20States";
    }
    
    if (_theLocation == NULL) {
        return;
    }
    
    [self refreshTableData];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
//    UIAlertView *errorAlert = [[UIAlertView alloc]
//                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [errorAlert show];
//    [_refreshControl endRefreshing];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"location_service_preference"]) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:@"location_service_preference"];
    }
    
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"locality_preference"] == NULL) {
        [[NSUserDefaults standardUserDefaults] setObject:@"Mountain%20View,United%20States" forKey:@"locality_preference"];
    }
    
    _theLocation = [[NSUserDefaults standardUserDefaults] stringForKey:@"locality_preference"];
    [self refreshTableData];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    if (_isUpadtingLocation) {
        return;
    }
    
    _isUpadtingLocation = true;
    CLLocation *currentLocation = newLocation;
    if (currentLocation != nil) {
        [self deviceLocation: currentLocation];
        [self.locationManager stopUpdatingLocation];
    } else {
        [_refreshControl endRefreshing];
    }
}

- (void) refreshTableData {
#warning population
    NSLog(@"_theLocation: %@", _theLocation);
    
    [[WeatherEntryManager sharedInstance] clearWeatherEntries];
    [[ConnectionManager sharedInstance] getWeatherEntries: [_theLocation stringByReplacingOccurrencesOfString:@" " withString:@"%20"] mode:@"json" units:@"metric" cnt:@"10"
                    completion:^(NSDictionary *weatherData, NSMutableArray *forecast) {
                        [[WeatherEntryManager sharedInstance] populateWeatherEntriesWithCompletion: forecast
                           completion: ^(BOOL success) {
                               _data =[[WeatherEntryManager sharedInstance] weatherEntries];
                               if (_refreshControl) {
                                   NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                   [formatter setDateFormat:@"MMM d, h:mm a"];
                                   NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
                                   NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                                               forKey:NSForegroundColorAttributeName];
                                   NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
                                   _refreshControl.attributedTitle = attributedTitle;
                                   [_refreshControl endRefreshing];
                               }
                               [_tableView reloadData];
                               _isUpadtingLocation = NO;
                               
                           }];
    }];
}

- (void) actionAdd: (id) sender {
    [[WeatherEntryManager sharedInstance] populateWeatherEntriesWithCompletion: nil completion: ^(BOOL success) {
        if (success) {
            _data = [[WeatherEntryManager sharedInstance] weatherEntries];
            [_tableView reloadData];
        }
    }];
}

#pragma mark - UITableView Datasource & Delegate -
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.row == 0 ? 132 : 44;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WeatherEntry *weatherEntry = _data[indexPath.row];
    static NSString *CellIdentifier = @"Cell";
    
    if (indexPath.row == 0) {
        WeatherEntryTodayTableViewCell *cell = (WeatherEntryTodayTableViewCell *)[tableView dequeueReusableCellWithIdentifier: CellIdentifier];
        
        //TODO: init here the blue TODAY layout
        if (cell == nil) {
            cell = [[WeatherEntryTodayTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        cell.forecastTextField.text = [NSString stringWithFormat:@"%@", weatherEntry.descriptionText];
        cell.maxTempTextField.text = [NSString stringWithFormat:@"%@%@", weatherEntry.max, @"°"];
        cell.minTempTextField.text = [NSString stringWithFormat:@"%@%@", weatherEntry.min, @"°"];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat: @"MMMM dd"];
        cell.dateTextField.text = [NSString stringWithFormat:@"%@, %@", @"Today", [formatter stringFromDate: weatherEntry.date]];
        
        //TODO: get the TODAY asset
        [cell.forecastIcon setImage: [UIImage imageNamed:
                                     [NSString stringWithFormat: @"%@%@",
                                      [[Utility sharedInstance] getIconResourceForWeatherCondition: [weatherEntry.status intValue]],
                                      @"_today"]]];

        cell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        return cell;
    } else {
        //TODO: usual layout
        WeatherEntryTableViewCell *cell = (WeatherEntryTableViewCell *)[tableView dequeueReusableCellWithIdentifier: CellIdentifier];
        if (cell == nil) {
            cell = [[WeatherEntryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        cell.forecastTextField.text = weatherEntry.descriptionText;
        cell.maxTempTextField.text = [NSString stringWithFormat:@"%@%@", weatherEntry.max, @"°"];
        cell.minTempTextField.text = [NSString stringWithFormat:@"%@%@", weatherEntry.min, @"°"];
        
        if (indexPath.row == 1) {
            cell.dateTextField.text = @"Tomorrow";
        } else {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat: @"EEEE"];
            cell.dateTextField.text = [formatter stringFromDate: weatherEntry.date];
        }
        
        cell.imageView.image = [UIImage imageNamed: [[Utility sharedInstance] getIconResourceForWeatherCondition: [weatherEntry.status intValue]]];
        
        return cell;
    }
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_data count];
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    if (_data.count) {
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        return 1;
    } else {
        _emptyContentLabel = [[UILabel alloc] initWithFrame:self.view.bounds];
        _emptyContentLabel.text = @"No data is currently available. Please pull down to refresh.";
        _emptyContentLabel.textColor = [UIColor blackColor];
        _emptyContentLabel.textAlignment = NSTextAlignmentCenter;
        [_emptyContentLabel sizeToFit];
        
        _tableView.backgroundView = _emptyContentLabel;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return 0;
}

#warning didSelectRowAtIndexPath
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    DetailViewController *vc = [[DetailViewController alloc] initWithWeatherEntry: _data[indexPath.row]];
    [vc setWeatherData: _data[indexPath.row]];

    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - UIBarButtonItems -
- (void)setupMenuBarButtonItems {
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu_icon"] style: UIBarButtonItemStylePlain
            target:self
            action:@selector(rightSideMenuButtonPressed:)];
}

#pragma mark - Right Bar Button Action -

- (void)rightSideMenuButtonPressed: (id) sender {
    if ([[_ver objectAtIndex:0] intValue] >= 9) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    } else if ([[_ver objectAtIndex:0] intValue] >= 8) {
        // iOS 8.0+
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    } else if ([[_ver objectAtIndex:0] intValue] >= 5) {
        // iOS 7 and earlier until iOS 5
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=General"]];
    }
    
//    [self.menuContainerViewController toggleLeftSideMenuCompletion: ^{}];
}

#pragma mark - Memory -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc {
    [_leftMenuButton removeFromSuperview];
    _leftMenuButton = nil;
    [_searchView removeFromSuperview];
    _searchView = nil;
    [_searchButton removeFromSuperview];
    _searchButton = nil;
    [_addButton removeFromSuperview];
    _addButton = nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
