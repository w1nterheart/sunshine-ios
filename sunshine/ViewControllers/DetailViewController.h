//
//  DetailViewController.h
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/15/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeatherEntry.h"

@interface DetailViewController : UIViewController

- (void) setWeatherData: (WeatherEntry *) weatherEntry;
- (id) initWithWeatherEntry: (WeatherEntry *) weatherEntry;

@end
