//
//  SideMenuViewController.m
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import "SideMenuViewController.h"
#import "MFSideMenu.h"

#import "HomeViewController.h"
//#import "LinkedinViewController.h"
//#import "ProfileViewController.h"
//#import "ContactsViewController.h"
//#import "TwitterViewController.h"
//#import "TWTViewController.h"
//#import "CropPhotoViewController.h"
//#import "ChatListViewController.h"
//#import "GooglePlusViewController.h"
//#import "InstagramViewController.h"
//#import "PersonListViewController.h"
//#import "BrowseGigsViewController.h"
#import "ProfileManager.h"

@implementation SideMenuViewController {
    UIImageView *_avatarImageView;
    UIImageView *_backgroundAvatarImageView;
    UILabel *_nameLabel;
}

static const CGFloat top_menu_margin = 25.f;
static const CGFloat avatar_background_height = 200.f;
static const CGFloat avatar_background_width = 270.f;
static const CGFloat avatar_background_blur_radius = 30.f;
static const CGFloat avatar_height = 120.f;
static const CGFloat name_label_height = 25.f;

static const CGFloat city_label_height = 40.f;
static const CGFloat text_font_size = 18.f;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:56./255. green:74./255. blue:84./255. alpha:1];
    
    _backgroundAvatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,
                                                                               avatar_background_width + avatar_background_blur_radius*4,
                                                                               avatar_background_height + avatar_background_blur_radius*4)];
    _backgroundAvatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    _backgroundAvatarImageView.alpha = .75;
    [self.view addSubview:_backgroundAvatarImageView];
    _backgroundAvatarImageView.center = CGPointMake(avatar_background_width/2, avatar_background_height/2);
    
    UIImage *inputImage = [[ProfileManager sharedInstance] profilePicture];
    CIImage *beginImage = [[CIImage alloc] initWithCGImage:inputImage.CGImage options:nil];
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur" keysAndValues: kCIInputImageKey, beginImage, @"inputRadius", @(avatar_background_blur_radius), nil];
    CIImage *outputImage = [filter outputImage];
    UIImage* _filteredImage = [UIImage imageWithCIImage:outputImage scale:[[UIScreen mainScreen] scale] orientation:inputImage.imageOrientation];
    _backgroundAvatarImageView.image = _filteredImage;
    
    _avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, avatar_height, avatar_height)];
    _avatarImageView.center = CGPointMake(avatar_background_width/2, avatar_background_height/2);
    _avatarImageView.image = inputImage;
    _avatarImageView.layer.cornerRadius = avatar_height/2;
    _avatarImageView.layer.masksToBounds = YES;
    _avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:_avatarImageView];
    
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_avatarImageView.frame) + 5, avatar_background_width, name_label_height)];
    _nameLabel.textAlignment = NSTextAlignmentCenter;
//    _nameLabel.text = [[ProfileManager sharedInstance] userFirstLastName];
    _nameLabel.text = @"Username";
    _nameLabel.textColor = [UIColor whiteColor];
    _nameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size: text_font_size];
    [self.view addSubview:_nameLabel];
    
//    _tableView = [[UITableView alloc] initWithFrame: self.view.bounds style:UITableViewStylePlain];
    _tableView = [[UITableView alloc] initWithFrame: CGRectMake(0, avatar_background_height + city_label_height, self.view.bounds.size.width, self.view.bounds.size.height - avatar_background_height + city_label_height + name_label_height)
                                              style:UITableViewStylePlain];
    [_tableView setContentInset:UIEdgeInsetsMake(top_menu_margin, 0, 0, 0)];
    
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundView = nil;
    _tableView.dataSource = self;
    _tableView.delegate = self;
//    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    [self.view addSubview:_tableView];
    // set default selected row
    [_tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
//
//    _textField = [[UITextField alloc] initWithFrame: CGRectMake(text_font_size, self.view.bounds.size.height - city_label_height, self.view.bounds.size.width, city_label_height)];
//    _textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString: @"Locality" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
//    _textField.textAlignment = NSTextAlignmentCenter;
//    _textField.textColor = [UIColor whiteColor];
//    _textField.font = [UIFont fontWithName:@"HelveticaNeue-Light" size: text_font_size];
//    [_textField setFont: [UIFont boldSystemFontOfSize: 12.0f]];
//    [_textField setTextAlignment:NSTextAlignmentLeft];
//    _textField.returnKeyType = UIReturnKeyDone;
//    _textField.delegate = self;
//    [self.view addSubview:_textField];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}



#pragma mark - UITableViewDataSource

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    return [NSString stringWithFormat:@"Section %d", (int)section];
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 11;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        cell.backgroundColor = [UIColor colorWithRed:39./255. green:52./255. blue:61./255. alpha:1.];
        UIView *selectedBackground = [[UIView alloc] initWithFrame:cell.frame];
        selectedBackground.backgroundColor = [UIColor colorWithRed:56./255. green:74./255. blue:84./255. alpha:1];
        cell.selectedBackgroundView = selectedBackground;
        
        cell.textLabel.textColor = [UIColor colorWithWhite:1 alpha:.6];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
        
        cell.imageView.image = [UIImage imageNamed:@"cell_menu_icon"];
    }
    
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = @"Home";
                break;
            case 1:
                cell.textLabel.text = @"Browse gigs";
                break;
            case 2:
                cell.textLabel.text = @"Connections";
                break;
            case 3:
                cell.textLabel.text = @"Linkedin";
                break;
            case 4:
                cell.textLabel.text = @"Twitter";
                break;
            case 5:
                cell.textLabel.text = @"Google + ";
                break;
            case 6:
                cell.textLabel.text = @"Instagram";
                break;
            case 7:
                cell.textLabel.text = @"Parse Contacts";
                break;
            case 8:
                cell.textLabel.text = @"Profile";
                break;
            case 9:
                cell.textLabel.text = @"Crop Photo";
                break;
            case 10:
                cell.textLabel.text = @"Chats";
                break;
            default:
                cell.textLabel.text = [NSString stringWithFormat:@"%d", (int)indexPath.row];
                break;
        }
    } else if (indexPath.section == 1) {
        cell.textLabel.text = [NSString stringWithFormat:@"%d", (int)indexPath.row];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0)
        return 0;
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.view endEditing:YES];
    
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
            {
                HomeViewController *vc = [[HomeViewController alloc] init];
                vc.title = @"Home";
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:vc];
                navigationController.viewControllers = controllers;
                break;
            }
            case 1:
            {
//                MBBrowseGigsViewController *vc = [[MBBrowseGigsViewController alloc] init];
//                vc.title = @"Browse Gigs";
//                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//                NSArray *controllers = [NSArray arrayWithObject:vc];
//                navigationController.viewControllers = controllers;
                break;
            }
            case 2:
            {
//                MBPersonListViewController *vc = [[MBPersonListViewController alloc] init];
//                vc.title = @"Connections";
//                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//                NSArray *controllers = [NSArray arrayWithObject:vc];
//                navigationController.viewControllers = controllers;
                break;
            }
            case 3:
            {
//                MBLinkedinViewController *vc = [[MBLinkedinViewController alloc] init];
//                vc.title = @"Linkedin";
//                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//                NSArray *controllers = [NSArray arrayWithObject:vc];
//                navigationController.viewControllers = controllers;
                break;
            }
            case 4:
            {
//                TWTViewController *vc = [[TWTViewController alloc] init];
//                vc.title = @"Twitter";
//                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//                NSArray *controllers = [NSArray arrayWithObject:vc];
//                navigationController.viewControllers = controllers;
                break;
            }
            case 5:
            {
//                MBGooglePlusViewController *vc = [[MBGooglePlusViewController alloc] init];
//                vc.title = @"Google +";
//                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//                NSArray *controllers = [NSArray arrayWithObject:vc];
//                navigationController.viewControllers = controllers;
                break;
            }
            case 6:
            {
//                MBInstagramViewController *vc = [[MBInstagramViewController alloc] init];
//                vc.title = @"Instagram";
//                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//                NSArray *controllers = [NSArray arrayWithObject:vc];
//                navigationController.viewControllers = controllers;
                break;
            }
            case 7:
            {
//                MBContactsViewController *vc = [[MBContactsViewController alloc] init];
//                vc.title = @"Contacts";
//                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//                NSArray *controllers = [NSArray arrayWithObject:vc];
//                navigationController.viewControllers = controllers;
                break;
            }
            case 8:
            {
//                ProfileViewController *vc = [[ProfileViewController alloc] init];
//                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//                NSArray *controllers = [NSArray arrayWithObject:vc];
//                navigationController.viewControllers = controllers;
                break;
            }
            case 9:
            {
//                MBCropPhotoViewController *vc = [[MBCropPhotoViewController alloc] init];
//                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//                NSArray *controllers = [NSArray arrayWithObject:vc];
//                navigationController.viewControllers = controllers;
                break;
            }
            case 10:
            {
//                MBChatListViewController *vc = [[MBChatListViewController alloc] init];
//                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//                NSArray *controllers = [NSArray arrayWithObject:vc];
//                navigationController.viewControllers = controllers;
                break;
            }
            default:
            {
                HomeViewController *vc = [[HomeViewController alloc] init];
                vc.title = [NSString stringWithFormat:@"Demo #%d-%d", (int)indexPath.section, (int)indexPath.row];
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:vc];
                navigationController.viewControllers = controllers;
                break;
            }
        }
    }
    
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == _tableView) {
        _avatarImageView.center = CGPointMake(avatar_background_width/2, -scrollView.contentOffset.y/2);
        _backgroundAvatarImageView.center = CGPointMake(avatar_background_width/2, -scrollView.contentOffset.y/2);
        _nameLabel.center = CGPointMake(avatar_background_width/2, CGRectGetMaxY(_avatarImageView.frame) + 5 + name_label_height/2);
//        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_avatarImageView.frame) + 5, avatar_background_width, name_label_height)];
        
        float delta = (-scrollView.contentOffset.y) - avatar_background_height;
        if ( delta > 0 ) {
            _backgroundAvatarImageView.layer.bounds = CGRectMake(0,
                                                                 0,
                                                                 (avatar_background_width + avatar_background_blur_radius*4) + delta,
                                                                 (avatar_background_height + avatar_background_blur_radius*4) + delta );
        }
    }
}

@end
