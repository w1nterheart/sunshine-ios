//
//  WeatherEntryTableViewCell.h
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/18/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeatherEntryTableViewCell : UITableViewCell

- (void) updateConstraints;

@property (strong, nonatomic) IBOutlet UIImage* forecastIcon;
@property (strong, nonatomic) IBOutlet UILabel *maxTempTextField;
@property (strong, nonatomic) IBOutlet UILabel *minTempTextField;
@property (strong, nonatomic) IBOutlet UILabel *dateTextField;
@property (strong, nonatomic) IBOutlet UILabel *forecastTextField;
@end
