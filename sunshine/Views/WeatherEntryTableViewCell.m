//
//  WeatherEntryTableViewCell.m
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/18/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import "WeatherEntryTableViewCell.h"
#import "WeatherEntry.h"
#import "WeatherEntryManager.h"

#import "Utility.h"

#define FORECAST_ICON_MARGIN ( 5.f )
#define FORECAST_ICON_SIDE ( 5.f )

@implementation WeatherEntryTableViewCell

@synthesize dateTextField = _dateTextField;
@synthesize forecastTextField = _forecastTextField;
@synthesize maxTempTextField = _maxTempTextField;
@synthesize minTempTextField = _minTempTextField;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    //TODO: resolve width on iPhone 6/6+
    if (self) {
        CGSize size = self.contentView.frame.size;
        
        self.dateTextField = [[UILabel alloc] initWithFrame: CGRectMake(64.0, 0, size.width - 24.0, size.height - 16.0)];
        [self.dateTextField setFont: [UIFont boldSystemFontOfSize: 12.0f]];
        [self.dateTextField setTextAlignment:NSTextAlignmentLeft];
        [self.dateTextField setTextColor:[UIColor blackColor]];
        [self addSubview: self.dateTextField];
        
        self.forecastTextField = [[UILabel alloc] initWithFrame: CGRectMake(64.0, 20.0, size.width - 24.0, size.height - 16.0)];
        [self.forecastTextField setFont: [UIFont fontWithName:@"HelveticaNeue" size: 10.0f]];
        [self.forecastTextField setTextAlignment:NSTextAlignmentLeft];
        [self.forecastTextField setTextColor:[UIColor grayColor]];
        [self addSubview: self.forecastTextField];
        
        self.maxTempTextField = [[UILabel alloc] initWithFrame: CGRectMake(8.0, 0, size.width - 24.0, size.height - 16.0)];
        [self.maxTempTextField setFont:[UIFont boldSystemFontOfSize:12.0f]];
        [self.maxTempTextField setTextAlignment:NSTextAlignmentRight];
        [self.maxTempTextField setTextColor:[UIColor blackColor]];
//        [self.maxTempTextField setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
        [self addSubview: self.maxTempTextField];
        
        self.minTempTextField = [[UILabel alloc] initWithFrame: CGRectMake(8.0, 20, size.width - 24.0, size.height - 16.0)];
        [self.minTempTextField setFont:[UIFont fontWithName:@"HelveticaNeue" size: 10.0f]];
        [self.minTempTextField setTextAlignment:NSTextAlignmentRight];
        [self.minTempTextField setTextColor:[UIColor grayColor]];
        self.minTempTextField.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        [self addSubview: self.minTempTextField];
        
        [self.contentView setTranslatesAutoresizingMaskIntoConstraints: NO];
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    }

    return self;
}

- (void) updateConstraints {
    [super updateConstraints];
    
    
//    NSDictionary *views = NSDictionaryOfVariableBindings(self.maxTempTextField, _minTempTextField);
}

@end
