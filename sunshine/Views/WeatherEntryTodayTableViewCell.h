//
//  WeatherEntryTodayTableViewCell.h
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/21/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeatherEntryTodayTableViewCell : UITableViewCell
- (void) updateConstraints;

@property (strong, nonatomic) IBOutlet UIImageView * forecastIcon;
@property (strong, nonatomic) IBOutlet UILabel *maxTempTextField;
@property (strong, nonatomic) IBOutlet UILabel *minTempTextField;
@property (strong, nonatomic) IBOutlet UILabel *dateTextField;
@property (strong, nonatomic) IBOutlet UILabel *forecastTextField;
@end
