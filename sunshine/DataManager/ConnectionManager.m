//
//  ConnectionManager.m
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/16/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import "ConnectionManager.h"
#import "NetworkActivity.h"

@implementation ConnectionManager

+ (ConnectionManager *) sharedInstance {
    static ConnectionManager* instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[ConnectionManager alloc] init];
    });
    return instance;
}

- (void) getWeatherEntries: (NSString *) location
                      mode: (NSString *) mode
                     units:(NSString *) units
                       cnt:(NSString *) cnt
                completion: (void (^)(NSDictionary *, NSMutableArray *)) completion {
    
    [[NetworkActivity sharedInstance] incrementNetworkActivity];
    NSString *baseUrl = [NSString stringWithFormat:@"%@", URI_FETCH_WEATHER];
    NSString *urlParams = [NSString stringWithFormat:@"/?q=%@&mode=%@&units=%@&cnt=%@", location, mode, units, cnt];
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@%@", baseUrl, urlParams]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod: @"GET"];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   if (data == nil)
                                       return;
            
                                   if (error)
                                       NSLog(@"ERROR: %@", [error localizedDescription]);
            
                                   NSError *err = nil;
                                   NSDictionary *dict = [NSJSONSerialization JSONObjectWithData: data
                                                                                        options: NSJSONReadingMutableContainers
                                                                                          error:&err];
            
                                   if (err)
                                       NSLog(@"ERROR: %@", [err localizedDescription]);
            
                                   NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*) response;
                                   NSInteger statusCode = [httpResponse statusCode];

                                   if (statusCode == STATUS_CODE_OK) {
                                       if (completion)
                                           completion(dict, (NSMutableArray *) dict[@"list"]);
                                   } else {
                                       if (completion)
                                           completion(dict, (NSMutableArray *) dict);
                                   }
                                   
                                   [[NetworkActivity sharedInstance] decrementNetworkActivity];
                               });
                           }];
}

@end
