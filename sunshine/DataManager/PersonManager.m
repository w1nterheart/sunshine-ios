//
//  PersonManager.m
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/15/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import "PersonManager.h"
#import "Person.h"

@implementation PersonManager

+ (PersonManager *)sharedInstance {
    static PersonManager* instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[PersonManager alloc] init];
    });
    return instance;
}

- (void)populatePersonsWithCompletion:(void (^)(BOOL))completion {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext){
            for (int i = 0; i < 10; i++) {
                Person *person = [Person MR_createEntityInContext:localContext];
                person.personID = [NSString stringWithFormat:@"id_%d", (int)(arc4random()*100)];
                person.firstName = @"Emilia";
                person.lastName = @"Clarke";
                person.username = @"username";
                person.photoURL = @"http://us.cdn2828.fansshare.com/photos/gameofthrones/emilia-clarke-wallpaper-game-of-thrones-girl-beautiful-portrait-face-girls-1466810484.jpg";
                person.isFriend = @YES;
            }
        } completion:^(BOOL success, NSError *error){
            if (completion)
                completion(success);
        }];
    });
}

- (NSMutableArray *)persons {
    NSMutableArray *persons = [[Person MR_findAllSortedBy:@"username" ascending:YES] mutableCopy];
    return persons;
}

- (NSMutableArray *)personsFriends:(BOOL)friendStatus {
    NSMutableArray *persons = [[Person MR_findByAttribute:@"isFriend"
                                               withValue:[NSNumber numberWithBool:friendStatus]
                                              andOrderBy:@"username"
                                               ascending:YES]
                               mutableCopy];
    return persons;
    
}

- (void)shareContactList {
    assert(0);
}

@end

