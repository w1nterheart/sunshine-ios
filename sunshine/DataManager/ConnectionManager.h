//
//  ConnectionManager.h
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/16/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectionManager : NSObject

+ (ConnectionManager *) sharedInstance;

/*!
 @method
 
 @param location, String for username.
 @param mode, String format parameter.
 @param units, String F or C degrees.
 @param cnt, String for count of entries.
 @param completion
 @param completion NSDictionary owmdata
 @param completion NSMutableArray it's the given listing of the node "list":
    @"city"
    @"name"
    @"coord"
    @"lat"
    @"lon"
    @"list"
        @"clouds"
        @"dt"
        @"pressure"
        @"humidity"
        @"speed"
        @"deg"
        @"temp"
            @"day"
            @"eve"
            @"max"
            @"min"
            @"morn"
            @"night"
        @"max"
        @"min"
        @"weather"
            @"description"
            @"icon"
            @"id"
            @"main"

 */
- (void) getWeatherEntries: (NSString *) location
                      mode: (NSString *) mode
                     units:(NSString *) units
                       cnt:(NSString *) cnt
                completion: (void (^)(NSDictionary *, NSMutableArray *)) completion;


@end
