//
//  WeatherEntryManager.h
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/15/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface WeatherEntryManager : NSManagedObject

+ (WeatherEntryManager *) sharedInstance;
- (void) populateWeatherEntriesWithCompletion: (NSMutableArray *) forecast
                                   completion: (void (^) (BOOL)) completion;
- (void) clearWeatherEntries;
- (NSMutableArray *) weatherEntries;

@end
