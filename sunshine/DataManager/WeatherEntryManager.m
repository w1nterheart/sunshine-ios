//
//  WeatherEntryManager.m
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/15/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import "WeatherEntryManager.h"
#import "WeatherEntry.h"

@implementation WeatherEntryManager

+ (WeatherEntryManager *) sharedInstance {
    static WeatherEntryManager* instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[WeatherEntryManager alloc] init];
    });
    return instance;
}

- (void) populateWeatherEntriesWithCompletion:  (NSMutableArray *) forecast completion: (void (^) (BOOL)) completion {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [MagicalRecord saveWithBlock: ^(NSManagedObjectContext *localContext) {

#warning here goes the deletion of all entities
            [WeatherEntry MR_truncateAllInContext: localContext];
            
            for (int i=0; i < [forecast count]; i++) {
                WeatherEntry *entry = [WeatherEntry MR_createEntityInContext:localContext];
                [entry MR_importValuesForKeysWithObject:forecast[i]];
                
                entry.descriptionText = [NSString stringWithFormat:@"%@", [forecast[i] valueForKeyPath:@"weather.main"][0]];
                
                entry.max = [NSNumber numberWithFloat: ceilf([[forecast[i] valueForKeyPath: @"temp.max"] floatValue])];
                entry.min = [NSNumber numberWithFloat: ceilf([[forecast[i] valueForKeyPath: @"temp.min"] floatValue])];
                entry.weatherID = [NSString stringWithFormat:@"id_%d", (int)(arc4random()*100)];
                
                entry.status = @([[forecast[i] valueForKeyPath: @"weather.id"][0] intValue]);
                NSTimeInterval timeInterval = (NSTimeInterval)[[forecast[i] valueForKeyPath:@"dt"] floatValue];
                entry.date = [NSDate dateWithTimeIntervalSince1970: timeInterval];
            }
        } completion: ^(BOOL success, NSError *error) {
            if (completion)
                completion(success);
        }];
    });
}

- (void) clearWeatherEntries {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [WeatherEntry MR_truncateAll];
    });
}

- (NSMutableArray *) weatherEntries {
    NSMutableArray *entries = [[WeatherEntry MR_findAllSortedBy:@"date" ascending:YES] mutableCopy];
#ifdef DEBUG
    NSLog(@"WeatherEntries count: \n%lu", (unsigned long)[entries count]);
#endif
    return entries;
}

@end
