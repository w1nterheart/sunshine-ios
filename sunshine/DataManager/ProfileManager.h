//
//  ProfileManager.h
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/15/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProfileManager : NSObject

@property (nonatomic, assign) BOOL isAuthenticated;
@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) UIImage *profilePicture;
@property (nonatomic, strong) NSString *userFirstLastName;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSNumber *userID;

+ (ProfileManager *)sharedInstance;
- (void)logout;

@end
