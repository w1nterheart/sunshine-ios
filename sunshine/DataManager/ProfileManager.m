//
//  ProfileManager.m
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/15/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import "ProfileManager.h"
#import "Settings.h"
//#import <FacebookSDK/FacebookSDK.h>
#import "KeychainItemWrapper.h"

static NSString * const kUserName = @"USERNAME";
static NSString * const kUserToken = @"USERTOKEN";

@implementation ProfileManager {
    KeychainItemWrapper *keychainItemWrapper;
}
@synthesize isAuthenticated = _isAuthenticated;

+ (ProfileManager *)sharedInstance {
    static ProfileManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[ProfileManager alloc] init];
        [instance setup];
        
    });
    return instance;
}

- (void)setup {
    keychainItemWrapper = [[KeychainItemWrapper alloc] initWithIdentifier:KEYCHAIN_WRAPPER_IDENTIFIER accessGroup:nil];
}

- (void)setIsAuthenticated:(BOOL)isAuthenticated {
    _isAuthenticated = isAuthenticated;
    [[Settings sharedInstance] setAuthenticated:isAuthenticated];
}

- (BOOL)isAuthenticated {
    // TEMPORARY SOLUTION
    return [[Settings sharedInstance] isAuthenticated];
}

- (void)logout {
    // TEMPORARY SOLUTION
    [[ProfileManager sharedInstance] setIsAuthenticated:NO];
    
//    FBSession* session = [FBSession activeSession];
//    [session closeAndClearTokenInformation];
//    [session close];
//    [FBSession setActiveSession:nil];
//    
//    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//    NSArray* facebookCookies = [cookies cookiesForURL:[NSURL URLWithString:@"https://facebook.com/"]];
//    
//    for (NSHTTPCookie* cookie in facebookCookies) {
//        [cookies deleteCookie:cookie];
//    }
    
    self.accessToken = nil;
    self.isAuthenticated = NO;
}

- (void)setProfilePicture:(UIImage *)profilePicture {
    // Directory for profile data
    //
    NSURL *documentProfileDirURL = [APPDELEGATE applicationDocumentsDirectory];
    documentProfileDirURL = [documentProfileDirURL URLByAppendingPathComponent:@"profile"];
    NSFileManager *fileManager= [NSFileManager defaultManager];
    BOOL isDir = YES;
    if(![fileManager fileExistsAtPath:[documentProfileDirURL path] isDirectory:&isDir])
        if(![fileManager createDirectoryAtPath:[documentProfileDirURL path] withIntermediateDirectories:YES attributes:nil error:NULL])
            NSLog(@"ERROR: Create folder failed %@", [documentProfileDirURL path]);
    
    // Saving the image into file
    //
    NSString *fileName = @"profile_picture";
    NSURL *fileURL = [[documentProfileDirURL URLByAppendingPathComponent:fileName] URLByAppendingPathExtension:@"png"];
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(profilePicture)];
    [imageData writeToFile:[fileURL path] atomically:YES];
}

- (UIImage *)profilePicture {
    
    NSURL *documentProfileDirURL = [APPDELEGATE applicationDocumentsDirectory];
    documentProfileDirURL = [documentProfileDirURL URLByAppendingPathComponent:@"profile"];
    NSString *fileName = @"profile_picture";
    NSURL *fileURL = [[documentProfileDirURL URLByAppendingPathComponent:fileName] URLByAppendingPathExtension:@"png"];
    UIImage *image = [UIImage imageWithContentsOfFile:[fileURL path]];
    
    if (!image)
        return [UIImage imageNamed:@"test_avatar"];
    else
        return image;
}

#pragma mark - User Defaults -

- (void)setUserName:(NSString *)userName {
    [[NSUserDefaults standardUserDefaults] setObject:userName forKey:USERNAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userName {
    if (![[NSUserDefaults standardUserDefaults] objectForKey:USERNAME])
        return @"noname";
    return [[NSUserDefaults standardUserDefaults] objectForKey:USERNAME];
}

- (void)setUserFirstLastName:(NSString *)userFirstLastName {
    [[NSUserDefaults standardUserDefaults] setObject:userFirstLastName forKey:USER_FIRST_LAST_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userFirstLastName {
    if (![[NSUserDefaults standardUserDefaults] objectForKey:USER_FIRST_LAST_NAME])
        return @"noname";
    return [[NSUserDefaults standardUserDefaults] objectForKey:USER_FIRST_LAST_NAME];
}

- (NSNumber *)userID {
    return @105;
}

#pragma mark - Keychain -

- (void)setAccessToken:(NSString *)accessToken {
    [keychainItemWrapper setObject:accessToken forKey:(__bridge id)kSecAttrAccount];
}

- (NSString *)accessToken {
    return (NSString *)[keychainItemWrapper objectForKey:(__bridge id)kSecAttrAccount];
}

@end
