//
//  Settings.m
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/15/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import "Settings.h"

@implementation Settings

+(Settings*) sharedInstance {
    static Settings *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[Settings alloc] init];
    });
    return instance;
}

- (BOOL) wasFirstStart {
    if (![[NSUserDefaults standardUserDefaults] objectForKey:FIRST_START]) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:FIRST_START];
    }
    return [[NSUserDefaults standardUserDefaults] boolForKey:FIRST_START];
}

- (void) setWasFirstStart: (BOOL) value {
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:FIRST_START];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL) isAuthenticated {
    if (![[NSUserDefaults standardUserDefaults] objectForKey:AUTHENTICATED]) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:AUTHENTICATED];
    }
    return [[NSUserDefaults standardUserDefaults] boolForKey:AUTHENTICATED];
}

- (void) setAuthenticated: (BOOL) value {
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:AUTHENTICATED];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
