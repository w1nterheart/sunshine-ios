//
//  PersonManager.h
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/15/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonManager : NSObject

+ (PersonManager *)sharedInstance;

- (void)populatePersonsWithCompletion:(void (^)(BOOL))completion;

- (NSMutableArray *)persons;
- (NSMutableArray *)personsFriends:(BOOL)friendStatus;

- (void)shareContactList;

@end
