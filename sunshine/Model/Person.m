//
//  Person.m
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/15/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import "Person.h"

@implementation Person

@dynamic firstName;
@dynamic lastName;
@dynamic username;
@dynamic photoURL;
@dynamic photoData;
@dynamic isFriend;
@dynamic personID;

@end
