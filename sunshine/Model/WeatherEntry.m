//
//  WeatherEntry.m
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/17/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import "WeatherEntry.h"


@implementation WeatherEntry

@dynamic date;
@dynamic dateText;
@dynamic degrees;
@dynamic humidity;
@dynamic locationID;
@dynamic pressure;
@dynamic descriptionText;
@dynamic status;
@dynamic min;
@dynamic max;
@dynamic weatherID;
@dynamic windSpeed;

@end
