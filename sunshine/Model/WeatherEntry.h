//
//  WeatherEntry.h
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/17/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class WeatherEntryManager;

@interface WeatherEntry : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * dateText;
@property (nonatomic, retain) NSString * descriptionText;
@property (nonatomic, retain) NSNumber * degrees;
@property (nonatomic, retain) NSNumber * humidity;
@property (nonatomic, retain) NSString * locationID;
@property (nonatomic, retain) NSNumber * pressure;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSNumber * min;
@property (nonatomic, retain) NSNumber * max;
@property (nonatomic, retain) NSString * weatherID;
@property (nonatomic, retain) NSNumber * windSpeed;
@property (nonatomic, retain) NSString * windDirection;

@end
