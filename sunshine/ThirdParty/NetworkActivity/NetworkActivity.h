//
//  MBNetworkActivity.h
//  mobilizr
//
//  Created by Stanislav Shpak on 10/10/14.
//  Copyright (c) 2014 grandiz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkActivity : NSObject

+ (NetworkActivity *)sharedInstance;

- (void)incrementNetworkActivity;
- (void)decrementNetworkActivity;

@end
