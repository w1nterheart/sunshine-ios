//
//  MBNetworkActivity.m
//  mobilizr
//
//  Created by Stanislav Shpak on 10/10/14.
//  Copyright (c) 2014 grandiz. All rights reserved.
//

#import "NetworkActivity.h"
#import <libkern/OSAtomic.h>

@implementation NetworkActivity
{
    int _networkActivityCounter;
}

+ (NetworkActivity *)sharedInstance
{
    static NetworkActivity *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[NetworkActivity alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Network activity

- (void)incrementNetworkActivity
{
    if (1 == OSAtomicIncrement32(&_networkActivityCounter))
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIApplication *app = [UIApplication sharedApplication];
            [app setNetworkActivityIndicatorVisible:YES];
        });
    }
}

- (void)decrementNetworkActivity
{
    if (0 == OSAtomicDecrement32(&_networkActivityCounter))
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIApplication *app = [UIApplication sharedApplication];
            [app setNetworkActivityIndicatorVisible:NO];
        });
    }
}

@end
